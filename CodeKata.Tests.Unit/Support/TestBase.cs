﻿using System.Threading;
using NUnit.Framework;

namespace CodeKata.Tests.Unit.Support
{
    [TestFixture]
    public abstract class TestBase
    {
        [OneTimeSetUp]
        protected void Init()
        {
            //ThreadPool.SetMinThreads(256, 8);
        }

        [SetUp]
        public virtual void Background()
        {
            Before_Test();
        }

        protected virtual void Before_Test()
        {
            
        }
    }
}