﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CodeKata.PathOfLowestCost.Extensions;
using CodeKata.PathOfLowestCost.Models;
using CodeKata.Tests.Unit.PathOfLowestCost.Support;
using CodeKata.Tests.Unit.Support;
using NUnit.Framework;

namespace CodeKata.Tests.Unit.PathOfLowestCost.Processes
{
    public class FindLowestPath : TestBase
    {
        [Test]
        public void Example_1_IterativeStack_Async()
        {
            var file = "example-one.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n16\n1 2 3 4 4 5", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Example_1_IterativeStack()
        {
            var file = "example-one.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n16\n1 2 3 4 4 5", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Example_1_Sync()
        {
            var file = "example-one.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n16\n1 2 3 4 4 5", ProcessEnum.Sync);
        }

        [Test]
        public void Example_2_IterativeStack_Async()
        {
            var file = "example-two.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n11\n1 2 1 5 4 5", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Example_2_IterativeStack()
        {
            var file = "example-two.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n11\n1 2 1 5 4 5", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Example_2_Sync()
        {
            var file = "example-two.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n11\n1 2 1 5 4 5", ProcessEnum.Sync);
        }

        [Test]
        public void Example_3_IterativeStack_Async()
        {
            var file = "example-three.txt".FindGridFilePath();
            
            TestExample(file, "No\n48\n1 1 1", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Example_3_IterativeStack()
        {
            var file = "example-three.txt".FindGridFilePath();
            
            TestExample(file, "No\n48\n1 1 1", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Example_3_Sync()
        {
            var file = "example-three.txt".FindGridFilePath();
            
            TestExample(file, "No\n48\n1 1 1", ProcessEnum.Sync);
        }

        [Test]
        public void Single_Line_No_Negatives_IterativeStack_Async()
        {
            var file = "single-line-no-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n24\n1 1 1 1 1 1", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Single_Line_No_Negatives_Iterative()
        {
            var file = "single-line-no-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n24\n1 1 1 1 1 1", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Single_Line_No_Negatives_Sync()
        {
            var file = "single-line-no-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n24\n1 1 1 1 1 1", ProcessEnum.Sync);
        }

        [Test]
        public void Single_Line_Negatives_Iterative()
        {
            var file = "single-line-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n22\n1 1 1 1 1 1", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Single_Line_Negatives_IterativeStack_Async()
        {
            var file = "single-line-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n22\n1 1 1 1 1 1", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Single_Line_Negatives_Sync()
        {
            var file = "single-line-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n22\n1 1 1 1 1 1", ProcessEnum.Sync);
        }

        [Test]
        public void Two_Lines_Negatives_IterativeStack_Async()
        {
            var file = "two-lines-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n14\n2 1 1 2 2 1", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Two_Lines_Negatives_Iterative()
        {
            var file = "two-lines-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n14\n2 1 1 2 2 1", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Two_Lines_Negatives_Sync()
        {
            var file = "two-lines-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n14\n2 1 1 2 2 1", ProcessEnum.Sync);
        }

        [Test]
        public void Mid_Range_Numbers_Negatives_Exactly_Fifty_Iterative()
        {
            var file = "mid-range-numbers-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n50\n1 1 1 2 3", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Mid_Range_Numbers_Negatives_Exactly_Fifty_IterativeStack_Async()
        {
            var file = "mid-range-numbers-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n50\n1 1 1 2 3", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Mid_Range_Numbers_Negatives_Exactly_Fifty_Sync()
        {
            var file = "mid-range-numbers-negatives.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n50\n1 1 1 2 3", ProcessEnum.Sync);
        }

        [Test]
        public void Mid_Grid_3x25_IterativeStack_Async()
        {
            Assert.Ignore("Passes, around 25 mins...");
            var file = "mid-grid-3x25.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n24\n1 2 1 1 3 1 2 1 1 3 1 2 2 1 3 1 2 2 1 3 1 2 1 1 3", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Mid_Grid_5x20_IterativeStack_Async()
        {
            var file = "mid-grid-5x20.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n-1\n4 5 1 2 3 4 4 3 3 4 5 4 5 5 5 4 4 5 5 5", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Mid_Grid_3x25_Iterative()
        {
            Assert.Ignore("Takes a while, so let's skip!");
            var file = "mid-grid-3x25.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n50\n1 1 1 2 3", ProcessEnum.IterativeStack);
        }

        [Test]
        public void Mid_Grid_3x25_Sync()
        {
            Assert.Ignore("Takes a while, so let's skip!");
            var file = "mid-grid-3x25.txt".FindGridFilePath();
            
            TestExample(file, "Yes\n50\n1 1 1 2 3", ProcessEnum.Sync);
        }

        [Test]
        public void Y_Big_Grid_3x100()
        {
            Assert.Ignore("The more columns, the much longer it takes to pass.  Exponential Growth vs CPU & time!");
            var file = "big-grid-3x100.txt".FindGridFilePath();

            TestExample(file, "", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Z_Big_Grid_10x100_1()
        {
            Console.WriteLine("Go grab a coffee or soda, you've got between 6-8mins");
            var file = "big-grid-10x100-1.txt".FindGridFilePath();

            TestExample(file, "No\n49\n1 2 1 10 10 1 2 3 4 3 4 4 3 4 5 6 7 7 6 5 4 3 3 4 5 6 7 6 6 5 4 3 3 4", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void Z_Big_Grid_10x100_2()
        {
            Assert.Ignore("Only run if you don't have anything to do for 10+ hrs.");

            var file = "big-grid-10x100-2.txt".FindGridFilePath();

            TestExample(file, "No\n49\n1 2 1 10 10 1 2 3 4 3 4 4 3 4 5 6 7 7 6 5 4 3 3 4 5 6 7 6 6 5 4 3 3 4", ProcessEnum.IterativeStackAsync);
        }

        [Test]
        public void A_No_Lines_File()
        {
            // We want this to run first so that we can elimate the spin up time
            var file = "no-lines.txt".FindGridFilePath();

            TestExample(file, "The Matrix is empty!", ProcessEnum.Sync, true);
        }

        private void TestExample(string file, string expectedOutput, ProcessEnum process = ProcessEnum.Async, bool emptyMatrix = false)
        {
            var watch = Stopwatch.StartNew();

            string message;
            var matrix = file.LoadMatrix(out message);

            if (!emptyMatrix)
            {
                Assert.That(matrix, Is.Not.Null, message);

                List<CkPath> paths;

                switch (process)
                {
                    case ProcessEnum.Async:
                        paths = matrix.FindPathsAsync();
                        break;
                    case ProcessEnum.Sync:
                        paths = matrix.FindPaths();
                        break;
                    case ProcessEnum.IterativeStackAsync:
                        paths = matrix.FindPathsIterativeAsync();
                        break;
                    case ProcessEnum.IterativeStack:
                        paths = matrix.FindPathsIterativeStack();
                        break;
                    default:
                        paths = new List<CkPath>();
                        break;
                }

                Assert.That(paths, Is.Not.Null);
                Assert.That(paths.Count, Is.GreaterThan(0));

                var lowestCostPath = paths.GetLowestCostPath();

                Assert.That(lowestCostPath, Is.Not.Null);

                var output = lowestCostPath.OutputLowestCostPath();

                Assert.That(output, Is.EqualTo(expectedOutput), output);

                Console.WriteLine(output);
            }
            else
            {
                Assert.That(matrix, Is.Null);

                Assert.That(message, Is.EqualTo(expectedOutput), message);

                Console.WriteLine(message);
            }

            watch.Stop();
            Console.WriteLine();
            Console.WriteLine("Time taken: {0}ms", watch.Elapsed.TotalMilliseconds);
            Console.WriteLine();
        }
    }
}