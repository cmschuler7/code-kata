﻿namespace CodeKata.Tests.Unit.PathOfLowestCost.Support
{
    public enum ProcessEnum
    {
        Sync,
        Async,
        IterativeStackAsync,
        IterativeStack
    }
}