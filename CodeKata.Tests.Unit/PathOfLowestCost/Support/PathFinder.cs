﻿using System;
using System.IO;

namespace CodeKata.Tests.Unit.PathOfLowestCost.Support
{
    public static class PathFinder
    {
        public static string FindGridFilePath(this string file)
        {
            return string.IsNullOrEmpty(file) ? string.Empty : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $@"PathOfLowestCost\GridFiles\{file}");
        }
    }
}