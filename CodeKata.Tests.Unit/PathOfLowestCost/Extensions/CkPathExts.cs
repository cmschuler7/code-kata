﻿using System;
using System.Collections.Generic;
using CodeKata.PathOfLowestCost.Models;
using CodeKata.PathOfLowestCost.Extensions;
using CodeKata.Tests.Unit.Support;
using NUnit.Framework;

namespace CodeKata.Tests.Unit.PathOfLowestCost.Extensions
{
    public class CkPathExts : TestBase
    {
        [Test]
        public void GetLowestCostPath_EmptyList_Returns_Null()
        {
            var paths = new List<CkPath>();

            var lowestPath = paths.GetLowestCostPath();

            Assert.That(lowestPath, Is.Null, "Not Null!");

            Console.WriteLine(lowestPath.OutputLowestCostPath());
        }

        [Test]
        public void GetLowestCostPath_Null_Returns_Null()
        {
            List<CkPath> paths = null;

            var lowestPath = paths.GetLowestCostPath();

            Assert.That(lowestPath, Is.Null, "Not Null!");

            Console.WriteLine(lowestPath.OutputLowestCostPath());
        }

        [Test]
        public void GetLowestCostPath_Finished_Fifty_Or_Under_Returns_Record()
        {
            var paths = new List<CkPath>
            {
                new CkPath {FinishLine = true, Steps = 3, TotalCost = 47, FinalPath = ",1,2,3"},
                new CkPath {FinishLine = true, Steps = 3, TotalCost = 48, FinalPath = ",2,2,3"},
                new CkPath {FinishLine = false, Steps = 2, TotalCost = 38, FinalPath = ",1,1"}
            };

            var lowestPath = paths.GetLowestCostPath();

            Assert.That(lowestPath, Is.Not.Null, "Null!");
            Assert.That(lowestPath.FinishLine, Is.True, "Not True");
            Assert.That(lowestPath.Steps, Is.EqualTo(3), "Not equal to 3");
            Assert.That(lowestPath.TotalCost, Is.EqualTo(47), "Not 47!");

            Console.WriteLine(lowestPath.OutputLowestCostPath());
        }

        [Test]
        public void GetLowestCostPath_Finished_Fifty_Or_Under_Same_Total_Returns_First_Found_Record()
        {
            var paths = new List<CkPath>
            {
                new CkPath {FinishLine = true, Steps = 3, TotalCost = 47, FinalPath = ",1,2,3"},
                new CkPath {FinishLine = true, Steps = 3, TotalCost = 48, FinalPath = ",2,2,3"},
                new CkPath {FinishLine = true, Steps = 3, TotalCost = 47, FinalPath = ",3,2,3"},
                new CkPath {FinishLine = false, Steps = 2, TotalCost = 38, FinalPath = ",1,1"}
            };

            var lowestPath = paths.GetLowestCostPath();

            Assert.That(lowestPath, Is.Not.Null, "Null!");
            Assert.That(lowestPath.FinishLine, Is.True, "Not True");
            Assert.That(lowestPath.Steps, Is.EqualTo(3), "Not equal to 3");
            Assert.That(lowestPath.TotalCost, Is.EqualTo(47), "Not 47!");
            Assert.That(lowestPath.FinalPath, Is.EqualTo(",1,2,3"), "Not equal!");

            Console.WriteLine(lowestPath.OutputLowestCostPath());
        }

        [Test]
        public void GetLowestCostPath_NotFinished_Returns_Furthest_Lowest_Path()
        {
            var paths = new List<CkPath>
            {
                new CkPath {FinishLine = false, Steps = 4, TotalCost = 47, FinalPath = ",1,2,3,3"},
                new CkPath {FinishLine = false, Steps = 3, TotalCost = 46, FinalPath = ",2,2,3"},
                new CkPath {FinishLine = false, Steps = 4, TotalCost = 48, FinalPath = ",3,2,3,3"},
                new CkPath {FinishLine = false, Steps = 2, TotalCost = 38, FinalPath = ",1,1"}
            };

            var lowestPath = paths.GetLowestCostPath();

            Assert.That(lowestPath, Is.Not.Null, "Null!");
            Assert.That(lowestPath.FinishLine, Is.False, "Not False");
            Assert.That(lowestPath.Steps, Is.EqualTo(4), "Not equal to 4");
            Assert.That(lowestPath.TotalCost, Is.EqualTo(47), "Not 47!");
            Assert.That(lowestPath.FinalPath, Is.EqualTo(",1,2,3,3"), "Not equal!");

            Console.WriteLine(lowestPath.OutputLowestCostPath());
        }

        [Test]
        public void OutputLowestCostPath_NullPath_Returns_NoPathFound()
        {
            CkPath lowestPath = null;

            var message = lowestPath.OutputLowestCostPath();

            Assert.That(message, Is.Not.Null);
            Assert.That(message, Is.Not.EqualTo(string.Empty));
            Assert.That(message, Is.EqualTo("No Path Found!"));

            Console.WriteLine(message);
        }

        [Test]
        public void OutputLowestCostPath_FinishedPath_Returns_Message()
        {
            var lowestPath = new CkPath {FinishLine = true, Steps = 3, TotalCost = 47, FinalPath = ",1,2,3"};

            var message = lowestPath.OutputLowestCostPath();

            Assert.That(message, Is.Not.Null);
            Assert.That(message, Is.Not.EqualTo(string.Empty));
            Assert.That(message, Is.EqualTo("Yes\n47\n1 2 3"));

            Console.WriteLine(message);
        }

        [Test]
        public void OutputLowestCostPath_NonFinishedPath_Returns_Message()
        {
            var lowestPath = new CkPath {FinishLine = false, Steps = 4, TotalCost = 47, FinalPath = ",1,2,3,3"};

            var message = lowestPath.OutputLowestCostPath();

            Assert.That(message, Is.Not.Null);
            Assert.That(message, Is.Not.EqualTo(string.Empty));
            Assert.That(message, Is.EqualTo("No\n47\n1 2 3 3"));

            Console.WriteLine(message);
        }
    }
}