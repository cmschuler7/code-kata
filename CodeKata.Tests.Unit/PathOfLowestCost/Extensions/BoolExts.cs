﻿using System;
using CodeKata.PathOfLowestCost.Extensions;
using CodeKata.Tests.Unit.Support;
using NUnit.Framework;

namespace CodeKata.Tests.Unit.PathOfLowestCost.Extensions
{
    public class BoolExts : TestBase
    {
        [Test]
        public void Bool_To_Yes_String()
        {
            const bool val = true;

            Assert.That(val.ToYesNoString(), Is.EqualTo("Yes"), "Value is false!");
            Console.WriteLine($"Boolean True = {val.ToYesNoString()}");
        }

        [Test]
        public void Bool_To_No_String()
        {
            const bool val = false;

            Assert.That(val.ToYesNoString(), Is.EqualTo("No"), "Value is true!");
            Console.WriteLine($"Boolean False = {val.ToYesNoString()}");
        }
    }
}