﻿using System;
using CodeKata.PathOfLowestCost.Extensions;
using CodeKata.Tests.Unit.PathOfLowestCost.Support;
using CodeKata.Tests.Unit.Support;
using NUnit.Framework;

namespace CodeKata.Tests.Unit.PathOfLowestCost.Extensions
{
    public class FileExts : TestBase
    {
        [Test]
        public void Can_Load_Matrix_From_File()
        {
            var file = "example-one.txt".FindGridFilePath();

            string message;
            var matrix = file.LoadMatrix(out message);

            Assert.That(matrix, Is.Not.Null, message);
            Assert.That(message, Is.EqualTo("Matrix Loaded!"));

            Console.WriteLine(message);
        }

        [Test]
        public void Cannot_Load_Matrix_From_Non_Existent_File()
        {
            var file = "";

            string message;
            var matrix = file.LoadMatrix(out message);

            Assert.That(matrix, Is.Null, message);
            Assert.That(message, Is.EqualTo("No file provided!"));

            Console.WriteLine(message);
        }

        [Test]
        public void Cannot_Load_Matrix_From_Empty_File()
        {
            var file = "no-lines.txt".FindGridFilePath();

            string message;
            var matrix = file.LoadMatrix(out message);

            Assert.That(matrix, Is.Null, message);
            Assert.That(message, Is.EqualTo("The Matrix is empty!"));

            Console.WriteLine(message);
        }

        [Test]
        public void Cannot_Load_Matrix_From_Not_Found_File()
        {
            var file = "not-found.txt".FindGridFilePath();

            string message;
            var matrix = file.LoadMatrix(out message);

            Assert.That(matrix, Is.Null, message);
            Assert.That(message, Is.EqualTo($"File not found: {file}"));

            Console.WriteLine(message);
        }
    }
}