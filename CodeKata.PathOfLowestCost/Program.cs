﻿using System;
using System.Diagnostics;
using CodeKata.PathOfLowestCost.Extensions;

namespace CodeKata.PathOfLowestCost
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var file = "test-grid.txt";
            if (args.Length > 0)
                file = args[0];

            var sw = Stopwatch.StartNew();

            string message;
            var matrix = file.LoadMatrix(out message);

            if (null == matrix)
            {
                sw.Stop();
                Console.WriteLine(message);
                Console.WriteLine();
                Console.WriteLine("Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);
                Console.ReadLine();
                return;
            }

            var lowestCostPath = matrix.FindPathsIterativeAsync().GetLowestCostPath().OutputLowestCostPath();

            sw.Stop();

            Console.WriteLine(lowestCostPath);
            Console.WriteLine();
            Console.WriteLine("Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);
            Console.ReadKey();
        }
    }
}