﻿using System.Collections.Generic;
using System.Linq;
using CodeKata.PathOfLowestCost.Models;
using CodeKata.PathOfLowestCost.Services;

namespace CodeKata.PathOfLowestCost.Extensions
{
    public static class ListIntMatrixEtx
    {
        public static List<CkPath> FindPathsIterativeAsync(this List<List<int>> matrix)
        {
            if (null == matrix || !matrix.Any())
                return null;

            var pathSrv = new PathSrv(matrix);

            return pathSrv.FindPathsIterativeAsync();
        }

        public static List<CkPath> FindPathsIterativeStack(this List<List<int>> matrix)
        {
            if (null == matrix || !matrix.Any())
                return null;

            var pathSrv = new PathSrv(matrix);

            return pathSrv.FindPathsIterativeStack();
        }

        public static List<CkPath> FindPathsAsync(this List<List<int>> matrix)
        {
            if (null == matrix || !matrix.Any())
                return null;

            var pathSrv = new PathSrv(matrix);

            return pathSrv.FindPathsAsync();
        }

        public static List<CkPath> FindPaths(this List<List<int>> matrix)
        {
            if (null == matrix || !matrix.Any())
                return null;

            var pathSrv = new PathSrv(matrix);

            return pathSrv.FindPaths();
        }
    }
}