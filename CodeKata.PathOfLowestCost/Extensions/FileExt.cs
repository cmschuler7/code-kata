﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeKata.PathOfLowestCost.Extensions
{
    public static class FileExt
    {
        public static List<List<int>> LoadMatrix(this string file, out string message)
        {
            if (string.IsNullOrEmpty(file))
            {
                message = "No file provided!";
                return null;
            }

            var matrix = new List<List<int>>();

            if (!File.Exists(file))
            {
                message = $"File not found: {file}";
                return null;
            }

            using (var sr = File.OpenText(file))
            {
                var line = "";
                while (null != (line = sr.ReadLine()))
                {
                    var rowInts = line.Split(',');

                    var ints = rowInts.Select(_ => Convert.ToInt32(_)).ToList();

                    matrix.Add(ints);
                }
            }

            if (matrix.Count == 0)
            {
                message = "The Matrix is empty!";
                return null;
            }

            message = "Matrix Loaded!";
            return matrix;
        }
    }
}