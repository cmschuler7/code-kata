﻿namespace CodeKata.PathOfLowestCost.Extensions
{
    public static class BoolExt
    {
        public static string ToYesNoString(this bool value)
        {
            return value ? "Yes" : "No";
        }
    }
}