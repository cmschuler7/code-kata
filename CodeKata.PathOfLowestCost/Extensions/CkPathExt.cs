﻿using System.Collections.Generic;
using System.Linq;
using CodeKata.PathOfLowestCost.Models;

namespace CodeKata.PathOfLowestCost.Extensions
{
    public static class CkPathExt
    {
        public static CkPath GetLowestCostPath (this List<CkPath> paths)
        {
            if (null == paths || !paths.Any())
                return null;

            var lowestPath = paths.Where(_ => _.Finished).OrderBy(_ => _.TotalCost).FirstOrDefault();

            if (null != lowestPath)
                return lowestPath;

            var lowestCost = paths.Where(_ => _.TotalCost < 50).OrderByDescending(_ => _.Steps).ThenBy(_ => _.TotalCost).FirstOrDefault();

            return lowestCost;
        }

        public static string OutputLowestCostPath(this CkPath path)
        {
            return null == path 
                ? "No Path Found!" 
                : $"{path.Finished.ToYesNoString()}\n{path.TotalCost}\n{path.FinalPath.TrimStart(',').Replace(',',' ')}";
        }
    }
}