﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeKata.PathOfLowestCost.Models;

namespace CodeKata.PathOfLowestCost.Services
{
    public class PathSrv
    {
        private int _highestStep;
        private int _lowestCost;
        private readonly int _totalRows;
        private readonly int _totalCols;
        private long _totalPaths;
        private bool _finishedPath;
        private readonly List<CkPath> _foundPaths;
        private readonly IReadOnlyList<List<int>> _matrix;

        public PathSrv(IReadOnlyList<List<int>> matrix)
        {
            _matrix = matrix;
            _totalRows = matrix?.Count ?? 0;
            _totalCols = matrix?[0].Count ?? 0;
            _foundPaths = new List<CkPath>();
        }

        public List<CkPath> FindPaths()
        {
            if (null == _matrix || !_matrix.Any())
                return null;

            for (var i = 0; i < _matrix.Count; i++)
            {
                FindPath(i, 0, new CkPath());
            }

            return _foundPaths;
        }

        public List<CkPath> FindPathsAsync()
        {
            if (null == _matrix || !_matrix.Any())
                return null;

            var tasks = new Task[_matrix.Count];

            for (var i = 0; i < _matrix.Count; i++)
            {
                var row = i;
                tasks[i] = Task.Run(() => FindPath(row, 0, new CkPath(), true));
            }

            Task.WaitAll(tasks);

            return _foundPaths;
        }

        public List<CkPath> FindPathsIterativeAsync()
        {
            if (null == _matrix || !_matrix.Any())
                return null;

            var tasks = new Task[_matrix.Count];

            for (var i = 0; i < _matrix.Count; i++)
            {
                var row = i;
                tasks[i] = Task.Run(() => FindPathIterativeStack(row, 0, new CkPath()));
            }

            Task.WaitAll(tasks);

            return _foundPaths;
        }

        public List<CkPath> FindPathsIterativeStack()
        {
            if (null == _matrix || !_matrix.Any())
                return null;

            for (var i = 0; i < _matrix.Count; i++)
            {
                FindPathIterativeStack(i, 0, new CkPath());
            }

            return _foundPaths;
        }

        private void FindPathIterativeStack(int row, int column, CkPath path)
        {
            var totalRows = _totalRows;
            var totalCols = _totalCols;

            var stackPath = new StackPath
            {
                CkPath = path,
                Column = column,
                Row = row
            };

            var stack = new Stack<StackPath>();
            stack.Push(stackPath);

            while (stack.Count > 0)
            {
                var currentStack = stack.Pop();

                if (currentStack.Row >= totalRows || currentStack.Column >= totalCols)
                    continue;

                var currentPath = currentStack.CkPath;
                var value = _matrix[currentStack.Row][currentStack.Column];
                var nextColumn = currentStack.Column + 1;
                var currentRow = currentStack.Row;

                currentPath.Steps++;
                currentPath.TotalCost += value;

                if (currentPath.OverBudget)
                {
                    _totalPaths++;
                    if (_finishedPath)
                        continue;

                    currentPath.Steps--;
                    currentPath.TotalCost -= value;

                    if (currentPath.Steps < _highestStep) continue;

                    if (currentPath.Steps > _highestStep)
                    {
                        _lowestCost = currentPath.TotalCost;
                        _highestStep = currentPath.Steps;
                        _foundPaths.Add(currentPath);
                    }
                    else
                    {
                        if (currentPath.TotalCost >= _lowestCost) continue;

                        _lowestCost = currentPath.TotalCost;
                        _foundPaths.Add(currentPath);
                    }

                    continue;
                }

                currentPath.FinalPath = $"{currentPath.FinalPath},{currentRow + 1}";

                if (totalCols == nextColumn)
                {
                    currentPath.FinishLine = true;
                    _totalPaths++;

                    if (!_finishedPath)
                    {
                        _lowestCost = currentPath.TotalCost;
                        _finishedPath = true;
                        _foundPaths.Add(currentPath);
                        continue;
                    }

                    if (currentPath.TotalCost >= _lowestCost) continue;

                    _lowestCost = currentPath.TotalCost;
                    _foundPaths.Add(currentPath);
                    continue;
                }

                if (totalRows == 1)
                {
                    stack.Push(new StackPath {CkPath = currentPath, Column = nextColumn, Row = currentRow});
                    continue;
                }

                if (totalRows == 2)
                {
                    // Copy Path so that we can keep what we already have and try another route!
                    var newPath = currentPath.ShallowCopy();
                    if (currentRow == 0)
                    {
                        stack.Push(new StackPath {CkPath = newPath, Column = nextColumn, Row = currentRow + 1});
                        stack.Push(new StackPath {CkPath = currentPath, Column = nextColumn, Row = currentRow});
                    }
                    else
                    {
                        stack.Push(new StackPath {CkPath = newPath, Column = nextColumn, Row = currentRow - 1});
                        stack.Push(new StackPath {CkPath = currentPath, Column = nextColumn, Row = currentRow});
                    }
                    continue;
                }

                var altPathUp = currentPath.ShallowCopy();
                var altPathDown = currentPath.ShallowCopy();

                stack.Push(currentRow + 1 == totalRows
                    ? new StackPath {CkPath = altPathUp, Column = nextColumn, Row = 0}
                    : new StackPath {CkPath = altPathUp, Column = nextColumn, Row = currentRow + 1});

                stack.Push(new StackPath { CkPath = currentPath, Column = nextColumn, Row = currentRow });

                stack.Push(currentRow == 0
                    ? new StackPath {CkPath = altPathDown, Column = nextColumn, Row = totalRows - 1}
                    : new StackPath {CkPath = altPathDown, Column = nextColumn, Row = currentRow - 1});
            }
        }

        private void FindPath(int row, int column, CkPath path, bool runAsync = false)
        {
            var totalRows = _matrix.Count;
            var totalColumns = _matrix[0].Count;
            var value = _matrix[row][column];
            var nextColumn = column + 1;

            path.Steps++;
            path.TotalCost += value;

            if (path.OverBudget)
            {
                _totalPaths++;
                if (_finishedPath)
                    return;

                path.Steps--;
                path.TotalCost -= value;

                if (path.Steps < _highestStep) return;

                if (path.Steps > _highestStep)
                {
                    _lowestCost = path.TotalCost;
                    _highestStep = path.Steps;
                    _foundPaths.Add(path);
                }
                else
                {
                    if (path.TotalCost >= _lowestCost) return;

                    _lowestCost = path.TotalCost;
                    _foundPaths.Add(path);
                }

                return;
            }

            path.FinalPath = $"{path.FinalPath},{row + 1}";

            if (totalColumns == nextColumn)
            {
                path.FinishLine = true;
                _totalPaths++;

                if (!_finishedPath)
                {
                    _lowestCost = path.TotalCost;
                    _finishedPath = true;
                    _foundPaths.Add(path);
                    return;
                }

                if (path.TotalCost >= _lowestCost) return;

                _lowestCost = path.TotalCost;
                _foundPaths.Add(path);
                return;
            }

            if (totalRows == 1)
            {
                FindPath(row, nextColumn, path);
                return;
            }

            if (totalRows == 2)
            {
                // Copy Path so that we can keep what we already have and try another route!
                var newPath = path.ShallowCopy();
                if (row == 0)
                {
                    // Current Row (0)
                    FindPath(row, nextColumn, path);
                    // Up Row (1)
                    FindPath(row + 1, nextColumn, newPath);
                }
                else
                {
                    // Current Row (1)
                    FindPath(row, nextColumn, path);
                    // Down Row (0)
                    FindPath(row - 1, nextColumn, newPath);
                }
                return;
            }

            var altPathUp = path.ShallowCopy();
            var altPathDown = path.ShallowCopy();

            if (runAsync)
            {
                // Up Row (Up is Down & Down is Up!)
                var tUp = row == 0
                    ? Task.Run(() => FindPath(totalRows - 1, nextColumn, altPathDown, true))
                    : Task.Run(() => FindPath(row - 1, nextColumn, altPathDown, true));

                // Current Row
                var tCurrent = Task.Run(() => FindPath(row, nextColumn, path, true));

                // Down Row
                var tDown = row + 1 == totalRows
                    ? Task.Run(() => FindPath(0, nextColumn, altPathUp, true))
                    : Task.Run(() => FindPath(row + 1, nextColumn, altPathUp, true));

                Task.WaitAll(tUp, tCurrent, tDown);
            }
            else
            {
                if (row == 0)
                {
                    FindPath(totalRows - 1, nextColumn, altPathDown);
                }
                else
                {
                    FindPath(row - 1, nextColumn, altPathDown);
                }

                FindPath(row, nextColumn, path);

                if (row + 1 == totalRows)
                {
                    FindPath(0, nextColumn, altPathUp);
                }
                else
                {
                    FindPath(row + 1, nextColumn, altPathUp);
                }
            }
        }

        private class StackPath
        {
            public int Column { get; set; }
            public int Row { get; set; }
            public CkPath CkPath { get; set; }
        }
    }
}