﻿namespace CodeKata.PathOfLowestCost.Models
{
    public class CkPath
    {
        public bool Finished => TotalCost <= 50 && FinishLine;
        public bool FinishLine { get; set; }
        public bool OverBudget => TotalCost > 50;
        public int TotalCost { get; set; }
        public int Steps { get; set; }
        public string FinalPath { get; set; }

        public CkPath ShallowCopy()
        {
            return (CkPath) MemberwiseClone();
        }
    }
}